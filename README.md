# HTML5 Banner Guide

## Steps

- Build Banner Creative
	- Build each animation state via discrete class wrapper (ie: .anim1, .anim2)
	- Build transitions between animation states (will not render in IE9)
		- No css3 animations, only transition
	- Add fallback creative with conditional comments (IE8 and below)
	- Add fallback creative in no-script tag
	- Hide all banner creative except no-script by default (enable with JS in init)

- Enable ISI
	- Use "new ISI()"
	- Configure isiOptions

- Add Exit Links
	- Add handlers (DOM.find, addEventListener, bind both click & touchend)
	- Add Enabler.exit("Description") calls
	- Optionally add any tracking data as second parameter to Enabler.exitQueryString

- Animate
	- Start animation sequence in init
	- Use setTimeout for each animation block
	- Set class of only parent container to each animation state (.anim1, .anim2)
	- Define delays and speeds in individual object transitions (will not render in IE9)
