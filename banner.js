(function() {
	window.Delegate = function(fn, context) {
		return function() {
			fn.apply(context, arguments);
		};
	};

	window.requestAnimationFrame = (window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.msRequestAnimationFrame || window.oRequestAnimationFrame || function (callback) { return window.setTimeout(callback, 60); });
	window.cancelRequestAnimationFrame = (window.cancelAnimationFrame || window.webkitCancelRequestAnimationFrame || window.mozCancelRequestAnimationFrame || window.msCancelRequestAnimationFrame || window.oCancelRequestAnimationFrame || window.clearTimeout);

	var DOM = {};

	DOM.create = function( str ) {
		var frag = document.createDocumentFragment();
		var elem = document.createElement('div');
		elem.innerHTML = str;
		while (elem.childNodes[0]) {
			frag.appendChild(elem.childNodes[0]);
		}
		return frag;
	};

	DOM.find = function( a, b ){
		var c = a.match(/^(\W)?(.*)/);
		var o;
		var select = "getElement" + ( c[1] ? c[1] === "#" ? "ById" : "sByClassName" : "sByTagName");
		o = ( b || document )[select]( c[2] )
		return o;
	};

	DOM.remove = function ( el ) {
		el = (typeof el === 'string') ? DOM.find(el) : el;
		if (el) {
			if (el.length) {
				var i = el.length; while (i--);
				if (el[i] && el[i].parentNode)
					el[i].parentNode.removeChild(el[i]);
			} else {
				el.parentNode.removeChild(el);
			}
		}
	};

	DOM.removeClass = function ( el, classname ) {
		el = (typeof el === 'string') ? DOM.find(el) : el;
		if (el) {
			var exp1 = /(?:^|\s)/;
			var exp2 = /(?!\S)/g;
			var exp  = new RegExp(exp1.source + classname + exp2.source);
			if (el.length) {
				var i=el.length; while (i--) {
					el[i].className = el[i].className.replace( exp, '' );
				}
			} else {
				el.className = el.className.replace( exp, '' );
			}
		}
	};

	DOM.addClass = function ( el, classname ) {
		el = (typeof el === 'string') ? DOM.find(el) : el;
		if (el) {
			if (el.length) {
				var i=el.length; while (i--) {
					if (el[i].className.indexOf(classname) === -1)
						el[i].className = (el[i].className === "") ? classname : el[i].className + " " + classname;
				}
			} else {
				if (!el.className || (el.className && el.className.indexOf(classname) === -1))
					el.className = (el.className === "") ? classname : el.className + " " + classname;
			}
		}
	}

	DOM.hasClass = function (el, classname) {
		el = (typeof el === 'string') ? DOM.find(el) : el;
		if (el) {
			var selector = " " + classname + " ";
			return ( (" " + el.className + " ").replace(/[\n\t]/g, " ").indexOf(selector) > -1 )
		}
		return false;
	}

	DOM.toggleClass = function ( el, classname ) {
		if ( DOM.hasClass(el, classname) ) {
			DOM.removeClass(el, classname);
		} else {
			DOM.addClass(el, classname);
		}
	}

	window.DOM = DOM;


/*global DOM*/
/*global Delegate*/
	// Create scrollbars in style of iScroll for easy styling
	var scrollbarHTML = '<div class="iScrollVerticalScrollbar iScrollLoneScrollbar" style="overflow: hidden;"><div class="iScrollIndicator"></div></div>';

	var has3d = (function(){
		var el = document.createElement('p'), has3d,
		transforms = {
			'webkitTransform':'-webkit-transform',
			'OTransform':'-o-transform',
			'msTransform':'-ms-transform',
			'MozTransform':'-moz-transform',
			'transform':'transform'
		};

		// Add it to the body to get the computed style
		document.body.insertBefore(el, null);

		for(var t in transforms){
			if (transforms.hasOwnProperty(t)) {
				if( el.style[t] !== undefined ){
					el.style[t] = 'translate3d(1px,1px,1px)';
					has3d = window.getComputedStyle(el).getPropertyValue(transforms[t]);
				}
			}
		}

		document.body.removeChild(el);

		return (has3d !== undefined && has3d.length > 0 && has3d !== "none");
	}());

	var ISI = function (id, options) {

		// Default Options
		this.isiOptions = {
			'mouseWheel': true,        // Enable mouse wheel
			'dragHandle': true,        // Enable click & drag on handle
			'dragTouch': true,         // Enable touch & drag on content
			'autoPlay': true,          // Enable autoplay
			'autoPlayStartDelay': 10,  // How long to delay before start of autoplay
			'autoPlayTime': 30,        // Time in seconds for 1 autoplay
			'autoPlayEndTime': 30,     // Total time before ending autoplay
			'autoPlayLoopDelay': null, // How long to pause between loops
			'autoPlayEndLoops': null   // Total loops before ending autoplay
		}

		// Heights & Y positions
		this.scrollerHeight = 0;
		this.containerHeight = 0;
		this.scrollerY = 0;
		this.indicatorY = 0;

		// Interval Trackers
		this.autoplayID = null;
		this.startDelayTimeout = null;

		// DOM elements
		this.scrollbar = DOM.create(scrollbarHTML);
		this.scroller = DOM.find(id);
		var parent = this.scroller.parentNode;
		if (!parent) return;

		// Overwrite default options with config
		if (options && typeof options === 'object') {
			for (var s in this.isiOptions) {
				if (options.hasOwnProperty(s)) {
					this.isiOptions[s] = options[s];
				}
			}
		}

		// Error handling for illegal autoPlay settings
		if (this.isiOptions['autoPlay'] && !this.isiOptions['autoPlayTime']) {
			this.isiOptions['autoPlayTime'] = 30;
		}

		// Get base heights for future calculations
		this.containerHeight = parent.clientHeight;
		this.scrollerHeight = this.scroller.clientHeight - this.containerHeight;
		parent.insertBefore(this.scrollbar, parent.childNodes[0]);
		this.scrollIndicator = DOM.find('.iScrollIndicator', parent)[0];
		this.dUpdatePositionRender = Delegate(this.updatePositionRender, this);
		this.updatePosition(0);

		// Mouse Wheel
		if (this.isiOptions['mouseWheel']) {
			this.wheelEvent = "onwheel" in document.createElement("div") ? "wheel" : // Modern browsers support "wheel"
				document.onmousewheel !== undefined ? "mousewheel" : // Webkit and IE support at least "mousewheel"
				"DOMMouseScroll"; // let's assume that remaining browsers are older Firefox
			var dWheelHandler = Delegate(this.handleMouseWheel, this);
			this.scroller.addEventListener(this.wheelEvent, dWheelHandler, false);
		}

		// Drag Handle
		if (this.isiOptions['dragHandle']) {
			this.dMouseDownHandler = Delegate(this.handleMouseDown, this);
			this.dMouseUpHandler = Delegate(this.handleMouseUp, this);
			this.dMouseMoveHandler = Delegate(this.handleMouseMove, this);
			this.scrollIndicator.addEventListener("mousedown", this.dMouseDownHandler, false);
			this.scrollIndicator.addEventListener("selectstart", function () {return false;}); // Block selections in IE
		}

		// Drag Touch
		if (this.isiOptions['dragTouch']) {
			this.dTouchStartHandler = Delegate(this.handleTouchStart, this);
			this.dTouchEndHandler = Delegate(this.handleTouchEnd, this);
			this.dTouchMoveHandler = Delegate(this.handleTouchMove, this);
			this.scroller.addEventListener("touchstart", this.dTouchStartHandler, false);
		}

		if (this.isiOptions['autoPlay']) {
			this.dAnimate = Delegate(this.animate, this);
			var delay = 1000;
			if (this.isiOptions['autoPlayStartDelay']) {
				delay = parseInt(this.isiOptions['autoPlayStartDelay'], 10) * 1000;
			}
			this.startDelayTimeout = setTimeout(this.dAnimate, delay);
		}

	};

	var p = ISI.prototype;

	p.updatePosition = function () {
		this.indicatorY = parseInt( -this.scrollerY / this.scrollerHeight * (this.containerHeight - 16), 10);
		// Throttle position updates faster than 60ms
		if (!this.updatePositionID) {
			this.updatePositionID = setTimeout(this.dUpdatePositionRender, 60);
		}
	};

	p.updatePositionRender = function () {
		clearTimeout(this.updatePositionID);
		delete this.updatePositionID;

		/*jshint multistr: true */
		var scrollerMove = '\
			-webkit-transition-timing-function: cubic-bezier(0.1, 0.57, 0.1, 1); \
			   -moz-transition-timing-function: cubic-bezier(0.1, 0.57, 0.1, 1); \
				-ms-transition-timing-function: cubic-bezier(0.1, 0.57, 0.1, 1); \
				 -o-transition-timing-function: cubic-bezier(0.1, 0.57, 0.1, 1); \
					transition-timing-function: cubic-bezier(0.1, 0.57, 0.1, 1); \
			-webkit-transition-duration: 30ms; \
			   -moz-transition-duration: 30ms; \
				-ms-transition-duration: 30ms; \
				 -o-transition-duration: 30ms; \
					transition-duration: 30ms; \
			-webkit-transform: translate(0px, ' + this.scrollerY + 'px)' + (has3d ? ' translateZ(0px);' : ';') + '\
			   -moz-transform: translate(0px, ' + this.scrollerY + 'px)' + (has3d ? ' translateZ(0px);' : ';') + '\
				-ms-transform: translate(0px, ' + this.scrollerY + 'px)' + (has3d ? ' translateZ(0px);' : ';') + '\
				 -o-transform: translate(0px, ' + this.scrollerY + 'px)' + (has3d ? ' translateZ(0px);' : ';') + '\
					transform: translate(0px, ' + this.scrollerY + 'px)' + (has3d ? ' translateZ(0px);' : ';') + '\
		';

		var scrollbarMove = '\
			display: block; \
			height: 8px; \
			-webkit-transition-duration: 30ms; \
			   -moz-transition-duration: 30ms; \
				-ms-transition-duration: 30ms; \
				 -o-transition-duration: 30ms; \
					transition-duration: 30ms; \
			-webkit-transform: translate(0px, ' + this.indicatorY + 'px)' + (has3d ? ' translateZ(0px);' : ';') + '\
			   -moz-transform: translate(0px, ' + this.indicatorY + 'px)' + (has3d ? ' translateZ(0px);' : ';') + '\
				-ms-transform: translate(0px, ' + this.indicatorY + 'px)' + (has3d ? ' translateZ(0px);' : ';') + '\
				 -o-transform: translate(0px, ' + this.indicatorY + 'px)' + (has3d ? ' translateZ(0px);' : ';') + '\
					transform: translate(0px, ' + this.indicatorY + 'px)' + (has3d ? ' translateZ(0px);' : ';') + '\
			-webkit-transition-timing-function: cubic-bezier(0.1, 0.57, 0.1, 1); \
			   -moz-transition-timing-function: cubic-bezier(0.1, 0.57, 0.1, 1); \
				-ms-transition-timing-function: cubic-bezier(0.1, 0.57, 0.1, 1); \
				 -o-transition-timing-function: cubic-bezier(0.1, 0.57, 0.1, 1); \
					transition-timing-function: cubic-bezier(0.1, 0.57, 0.1, 1); \
		';

		this.scroller.setAttribute('style', scrollerMove);
		this.scrollIndicator.setAttribute('style', scrollbarMove);
	}

	p.animate = function () {
		this.autoplayID = requestAnimationFrame(this.dAnimate);

		if (!this.animateStartTime) this.animateStartTime = new Date().getTime();

		var pt = this.isiOptions['autoPlayTime'] * 1000;
		var pld = (this.isiOptions['autoPlayLoopDelay'] * 1000) || 0;

		var t = new Date().getTime();
		var diff = (t - this.animateStartTime) || 0;
		var loop = Math.floor( diff / ( pt + pld ));

		var totalLoopTime = (loop+1) * (pt + pld);
		var totalTimeSansLoop = totalLoopTime - pld;

		if (diff >= totalTimeSansLoop && diff <= totalLoopTime) {
			return; // Pause state
		}

		var progress = (diff - (loop * (pt + pld))) / pt;

		this.scrollerY = -Math.round(this.scrollerHeight * progress);
		this.updatePosition();

		// End if time progress exceeded
		if (this.isiOptions['autoPlayEndTime']) {
			var timeoutProgress = diff / (this.isiOptions['autoPlayEndTime'] * 1000);
			if (timeoutProgress >= 1) {
				window.cancelRequestAnimationFrame(this.autoplayID);
			}
		}

		// End if loops exceeded
		if (this.isiOptions['autoPlayEndLoops'] !== null) {
			if (loop > this.isiOptions['autoPlayEndLoops']) {
				window.cancelRequestAnimationFrame(this.autoplayID);
				this.scrollerY = 0;
				this.updatePosition();
			}
		}
	};

	p.handleMouseWheel = function (e) {
		e = window.event || e;
		window.cancelRequestAnimationFrame(this.autoplayID);
		clearTimeout(this.startDelayTimeout);
		delete this.startDelayTimeout;
		var delta = e.wheelDelta || -e.detail;
		this.scrollerY = Math.max(-this.scrollerHeight, Math.min(0, this.scrollerY +delta));
		this.updatePosition();
	};

	p.handleMouseDown = function (e) {
		e = window.event || e;
		window.cancelRequestAnimationFrame(this.autoplayID);
		clearTimeout(this.startDelayTimeout);
		delete this.startDelayTimeout;
		this.mouseClientYStart = e.clientY;
		this.indicatorYStart = this.indicatorY;
		window.addEventListener("mouseup", this.dMouseUpHandler, false);
		window.addEventListener("mousemove", this.dMouseMoveHandler, false);
	};

	p.handleMouseUp = function (e) {
		e = window.event || e;
		window.removeEventListener("mouseup", this.dMouseUpHandler, false);
		window.removeEventListener("mousemove", this.dMouseMoveHandler, false);
	};

	p.handleMouseMove = function (e) {
		var newY = e.clientY;
		var delta = newY - this.mouseClientYStart || 0;
		var newIndicatorY = Math.min(this.containerHeight - 16, Math.max(0, this.indicatorYStart + delta));
		this.scrollerY = newIndicatorY / (this.containerHeight - 16) * -this.scrollerHeight;
		this.updatePosition();
		e.preventDefault();
		return false;
	};

	p.handleTouchStart = function (e) {
		window.cancelRequestAnimationFrame(this.autoplayID);
		clearTimeout(this.startDelayTimeout);
		delete this.startDelayTimeout;
		this.touchClientY = e.touches[0].clientY;
		window.addEventListener("touchend", this.dTouchEndHandler, false);
		window.addEventListener("touchmove", this.dTouchMoveHandler, false);
	};

	p.handleTouchEnd = function (e) {
		window.removeEventListener("touchend", this.dTouchEndHandler, false);
		window.removeEventListener("touchmove", this.dTouchMoveHandler, false);
	};

	p.handleTouchMove = function (e) {
		var newY = e.touches[0].clientY;
		var delta = this.touchClientY - newY || 0;
		this.touchClientY = newY;
		this.scrollerY = Math.max(-this.scrollerHeight, Math.min(0, this.scrollerY - delta));
		this.updatePosition();
		e.preventDefault();
		return false;
	};

	p.showFullISI = function (e) {
		container = DOM.find('#main-container');
		DOM.removeClass(container, "enabled"); // Show banner
		var isi_inner_content = DOM.find('.isi-content', this.scroller)[0];
		var full_screen_isi = DOM.create("<div id='fullscreenisi'></div>");
		full_screen_isi.appendChild(isi_inner_content);
		document.body.appendChild(full_screen_isi);
	};

	window.ISI = ISI;
})();
