/*global DOM*/
/*global ISI*/
/*global studio*/
/*global Enabler*/


var container;
var isiPrescribingInfoExit;
var isiMedGuideExit;
var clickTagEl;


function onStaticClickTag() {
	Enabler.exit('Static CTA Exit');
}

function onClickTag() {
	Enabler.exit('CTA Exit');
}

function onISIPrescribingInfo() {
	Enabler.exit('ISI Prescribing Info Exit');
}

function onISIMedGuide() {
	Enabler.exit('ISI Med Guide Exit');
}


function initializeBanner() {

	// Cheap test if we're in IE9+
	if(  !document.addEventListener  ){
		clickTagEl = document.getElementById("staticClickTag");
		clickTagEl.attachEvent("onclick", onStaticClickTag);

	// Modern Browser
	} else {

		/************************ Show Banner ****************************/

		container = DOM.find('#container');
		DOM.addClass(container, "enabled"); // Show banner
		if (typeof container.onselectstart!=="undefined") { // Disable IE9 highlight
			container.onselectstart=function(){return false};
		}

		/**************************** ISI ********************************/

		// ISI configuration
		var isiOptions = {
			'mouseWheel': true,      // Enable mouse wheel
			'dragHandle': true,      // Enable click & drag on handle
			'dragTouch': true,       // Enable touch & drag on content
			'autoPlay': true,        // Enable autoplay
			'autoPlayStartDelay': 8, // Delay before starting autoplay
			'autoPlayTime': 200,     // Time in seconds for 1 autoplay
			'autoPlayEndTime': null, // Total time before ending autoplay
			'autoPlayLoopDelay': 5,  // How long to pause between loops
			'autoPlayEndLoops': 0    // Total loops before ending autoplay
		}
		var isi = new ISI('#scroller', isiOptions);

		/************************* Exit Links ********************************/

		// Get references to all objects
		clickTagEl = DOM.find("#clickTag");
		isiPrescribingInfoExit = DOM.find("#isi_prescribing_exit");
		isiMedGuideExit = DOM.find("#isi_medguide_exit");

		// Add exit listeners
		clickTagEl.addEventListener('click', onClickTag, false);
		clickTagEl.addEventListener('touchend', onClickTag, false);
		isiPrescribingInfoExit.addEventListener('click', onISIPrescribingInfo, false);
		isiPrescribingInfoExit.addEventListener('touchend', onISIPrescribingInfo, false);
		isiMedGuideExit.addEventListener('click', onISIMedGuide, false);
		isiMedGuideExit.addEventListener('touchend', onISIMedGuide, false);

		/*************************** Animation ********************************/
		DOM.addClass(container, 'state1');

		setTimeout(function() {
			DOM.removeClass(container, 'state1');
			DOM.addClass(container, 'state2');
		}, 4000);

		setTimeout(function() {
			DOM.removeClass(container, 'state2');
			DOM.addClass(container, 'state3');
		}, 8000);

		setTimeout(function() {
			DOM.removeClass(container, 'state3');
			DOM.addClass(container, 'state4');
		}, 12000);

		setTimeout(function() {
			DOM.removeClass(container, 'state4');
			DOM.addClass(container, 'state5');
		}, 16000);
	}
}


/* Doubleclick Enabler Polite Load code waits to build banner until:
 * - Enabler fully loaded
 * - Web page fully loaded
 * - Banner creative visible on screen
 */
function enablerPageLoadedHandler() {
	if (Enabler.isVisible()) {
		initializeBanner();
	} else {
		Enabler.addEventListener( studio.events.StudioEvent.VISIBLE,
			initializeBanner);
	}
}

function enablerInitHandler() {
	if (Enabler.isInitialized()) {
		enablerPageLoadedHandler();
	} else {
		Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED,
			enablerPageLoadedHandler);
	}
}

window.onload = function() {
	if (Enabler.isInitialized()) {
		enablerInitHandler();
	} else {
		Enabler.addEventListener(studio.events.StudioEvent.INIT,
			enablerInitHandler);
	}
}
